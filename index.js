const createSteps = require('./steps')
const { default: imageProcessing, getColor } = require('responsimage')

function getSteps(name) {
  const previewName = `${name}_preview`
  const fullPreviewName = `${name}_full_preview`
  return createSteps(previewName, fullPreviewName)
}

async function createFeaturedImage({ source, name, dir, ...opts }) {
  const steps = getSteps(name)
  return await imageProcessing(source, {
    steps,
    dir,
    ...opts,
  })
}

function getFilenames(name) {
  return getSteps(name)
    .reduce((acc, next) => {
      const { name, suffix } = next
      const filename = `${name}${suffix ? suffix : ''}`
      return [
        ...acc,
        ...(suffix !== '_tiny'
          ? [
              { filename, ext: 'jpg', suffix },
              { filename, ext: 'webp', suffix },
            ]
          : [{ filename, ext: 'jpg', suffix }]),
      ]
    }, [])
    .map(file => `${file.filename}.${file.ext}`)
}

module.exports = {
  createFeaturedImage,
  getSteps,
  getFilenames,
  imageProcessing,
  getColor,
}
