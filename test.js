const { createFeaturedImage } = require('./index')

createFeaturedImage({
  source: './test.jpg',
  name: 'test_123',
  dir: './output_test',
  quiet: true,
})
  .then(({ images, color: { hex, rgb } }) => {
    console.log({ images, hex, rgb })
  })
  .catch(err => console.log({ err }))
