module.exports = function(previewName, fullPreviewName) {
  return [
    {
      stepName: 'Preview Image BIG 2x',
      size: [960, 836],
      name: previewName,
      suffix: '_retina',
    },
    {
      stepName: 'Preview Image BIG 1x',
      name: previewName,
      size: [540, 470],
    },
    // {
    //   stepName: 'Preview Image SMALL 2x',
    //   size: [750, 653],
    //   name: previewName,
    //   suffix: '_small_retina',
    // },
    // {
    //   stepName: 'Preview Image SMALL 1x',
    //   size: [375, 326],
    //   name: previewName,
    //   suffix: '_small',
    // },
    // {
    //   stepName: 'Tiny Preview Image',
    //   size: [27, 23],
    //   name: previewName,
    //   suffix: '_tiny',
    //   webp: false,
    // },
    {
      stepName: 'Full Preview Image BIG 2x',
      size: [1920],
      name: fullPreviewName,
      suffix: '_retina',
    },
    {
      stepName: 'Full Preview Image BIG 1x',
      size: [960],
      name: fullPreviewName,
    },
    // {
    //   stepName: 'Full Preview Image SMALL 2x',
    //   size: [750],
    //   name: fullPreviewName,
    //   suffix: '_small_retina',
    // },
    // {
    //   stepName: 'Full Preview Image SMALL 1x',
    //   size: [375],
    //   name: fullPreviewName,
    //   suffix: '_small',
    // },
  ]
}
